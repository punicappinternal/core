package com.punicapp.sample;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.punicapp.core.masked.MaskedFormatter;
import com.punicapp.core.network.NetworkStateReceiver;
import com.punicapp.core.utils.DateManager;

import java.text.ParseException;
import java.util.Date;
import java.util.concurrent.Executors;

import io.reactivex.functions.Consumer;

public class MainActivity extends AppCompatActivity {

    private NetworkStateReceiver nsr;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        View hT = findViewById(R.id.hello_txt);

        nsr = new NetworkStateReceiver(this);
        nsr.listen().subscribe(new Consumer<Boolean>() {
            @Override
            public void accept(Boolean aBoolean) throws Exception {
                Toast toast = Toast.makeText(MainActivity.this, "Is connected " + aBoolean, Toast.LENGTH_SHORT);
                toast.show();
            }
        });
        String s = DateManager.getInstance().formatModelDate(new Date(System.currentTimeMillis()));

    }

    @Override
    protected void onResume() {
        super.onResume();
        nsr.register();
    }

    @Override
    protected void onPause() {
        super.onPause();
        nsr.unregister();
    }
}
