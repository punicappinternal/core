package com.punicapp.sample;

import android.content.Context;
import android.content.res.Resources;
import android.support.test.InstrumentationRegistry;
import android.util.DisplayMetrics;
import android.util.TypedValue;

import com.punicapp.core.utils.DimensUtils;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Arrays;
import java.util.Collection;

import static org.junit.Assert.*;

/**
 * Instrumentation test, which will execute on an Android device.
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */

@RunWith(Parameterized.class)
public class DimensUtilsInstrumentedTest {
    private static final int PARAMS_COUNT = 2;
    private static final int TESTS_COUNT = 20;

    @Parameterized.Parameter(value = 0)
    public int pixelValues;

    @Parameterized.Parameter(value = 1)
    public int dpValues;

    @Parameterized.Parameters
    public static Collection<Object[]> initParameters() {
        Object[][] testData = new Object[TESTS_COUNT][PARAMS_COUNT];

        for (int i = 0; i < TESTS_COUNT; i++) {
            testData[i][0] = i * 100;
            testData[i][1] = i * 10;
        }

        return Arrays.asList(testData);
    }

    @Test
    public void testDpToPxCorrectness() {
        Context appContext = InstrumentationRegistry.getTargetContext();
        Resources resources = appContext.getResources();

        try {
            int expectedValue = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dpValues, resources.getDisplayMetrics());
            int actualValue = DimensUtils.dpToPx(dpValues);

            assertEquals(expectedValue, actualValue);
        } catch (Exception exception) {
            fail("Exception not handling! " + exception.getMessage());
        }
    }

    @Test
    public void testPxToDpCorrectness() {
        Context appContext = InstrumentationRegistry.getTargetContext();
        Resources resources = appContext.getResources();

        try {
            int expectedValue = (int)(pixelValues / ((float)resources.getDisplayMetrics().densityDpi / DisplayMetrics.DENSITY_DEFAULT));
            int actualValue = DimensUtils.pxToDp(pixelValues);

            assertEquals(expectedValue, actualValue);
        } catch (Exception exception) {
            fail("Exception not handling! " + exception.getMessage());
        }

    }

}
