package com.punicapp.sample;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

/**
 * Created by Evgeny on 18.01.18.
 */

@RunWith(Suite.class)
@Suite.SuiteClasses({StringUtilsTest.class})
public class UnitTestSuite {}

