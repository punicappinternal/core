package com.punicapp.sample;

import com.punicapp.core.utils.DateFormatter;
import com.punicapp.core.utils.DateManager;
import com.punicapp.core.utils.FormatType;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

import static org.junit.Assert.*;

/**
 * Created by Evgeny on 17.01.18.
 */
@RunWith(Parameterized.class)
public class DateManagerTest {

    private DateManager dateManager = DateManager.getInstance();

    @Parameterized.Parameter(value = 0)
    public Date dateValue;

    @Parameterized.Parameter(value = 1)
    public String serverFormatDateString;

    @Parameterized.Parameter(value = 2)
    public String shortServerFormatDateString;

    @Parameterized.Parameter(value = 3)
    public String modelFormatDateString;

    @Parameterized.Parameter(value = 4)
    public String shortModelFormatDateString;

    @Parameterized.Parameter(value = 5)
    public DateFormat customDateFormat;

    @Parameterized.Parameter(value = 6)
    public String customFormatDateString;

    @Parameterized.Parameters
    public static Collection<Object[]> initParameters() {

        List<Calendar> calendarList = new ArrayList<>();
        Calendar calendarOne = getDate(2018, 1, 29);
        calendarOne.add(Calendar.DATE, 1);
        calendarList.add(calendarOne);
        Calendar calendarTwo = getDate(2018, 1, 29);
        calendarTwo.add(Calendar.DATE, 7);
        calendarList.add(calendarTwo);
        Calendar calendarThree = getDate(2018, 1, 29);
        calendarThree.add(Calendar.HOUR, 12);
        calendarList.add(calendarThree);
        Calendar calendarFour = getDate(2018, 1, 29);
        calendarFour.add(Calendar.YEAR, 1);
        calendarList.add(calendarFour);
        Calendar calendarFive = getDate(2018, 1, 29);
        calendarFive.add(Calendar.MINUTE, 30);
        calendarList.add(calendarFive);
        Calendar calendarSix = getDate(2018, 1, 29);
        calendarSix.add(Calendar.SECOND, 30);
        calendarList.add(calendarSix);
        Calendar calendarSeven = getDate(2018, 1, 29);
        calendarSeven.add(Calendar.DATE, 365);
        calendarList.add(calendarSeven);
        Calendar calendarEight = getDate(2018, 1, 29);
        calendarEight.add(Calendar.HOUR, 72);
        calendarList.add(calendarEight);
        Calendar calendarNine = getDate(2018, 1, 29);
        calendarNine.add(Calendar.DATE, 30);
        calendarList.add(calendarNine);
        calendarList.add(null);

        return Arrays.asList(new Object[][] {
                {calendarList.get(0).getTime(), "2018-03-02 00:00:00", "2018-03-02", "02.03.2018 00:00", "02.03.2018", new SimpleDateFormat("yyyy.MM.dd G 'at' HH:mm:ss z"), "2018.03.02 н.э. at 00:00:00 NOVT"},
                {calendarList.get(1).getTime(), "2018-03-08 00:00:00", "2018-03-08", "08.03.2018 00:00", "08.03.2018", new SimpleDateFormat("EEE, MMM d, ''yy"), "Чт, мар 8, '18" },
                {calendarList.get(2).getTime(), "2018-03-01 12:00:00", "2018-03-01", "01.03.2018 12:00", "01.03.2018", new SimpleDateFormat("h:mm a"), "12:00 PM" },
                {calendarList.get(3).getTime(), "2019-03-01 00:00:00", "2019-03-01", "01.03.2019 00:00", "01.03.2019", new SimpleDateFormat("hh 'o''clock' a, zzzz"), "12 o'clock AM, Novosibirsk Time" },
                {calendarList.get(4).getTime(), "2018-03-01 00:30:00", "2018-03-01", "01.03.2018 00:30", "01.03.2018", new SimpleDateFormat("K:mm a, z"), "0:30 AM, NOVT" },
                {calendarList.get(5).getTime(), "2018-03-01 00:00:30", "2018-03-01", "01.03.2018 00:00", "01.03.2018", new SimpleDateFormat("yyyyy.MMMMM.dd GGG hh:mm aaa"), "02018.марта.01 н.э. 12:00 AM" },
                {calendarList.get(6).getTime(), "2019-03-01 00:00:00", "2019-03-01", "01.03.2019 00:00", "01.03.2019", new SimpleDateFormat("EEE, d MMM yyyy HH:mm:ss Z") , "Пт, 1 мар 2019 00:00:00 +0600"},
                {calendarList.get(7).getTime(), "2018-03-04 00:00:00", "2018-03-04", "04.03.2018 00:00", "04.03.2018", new SimpleDateFormat("yyMMddHHmmssZ"), "180304000000+0600" },
                {calendarList.get(8).getTime(), "2018-03-31 00:00:00", "2018-03-31", "31.03.2018 00:00", "31.03.2018", new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ"), "2018-03-31T00:00:00.000+0600" },
                {null, "", "", "", "", null, "" }});
    }

    @Test
    public void testFormatServerDate() {
        try {
            String actualValue = dateManager.formatServerDate(dateValue);
            assertEquals(serverFormatDateString, actualValue);
        } catch (Exception exception) {
            fail("Exception not handling " + exception.getMessage());
        }
    }

    @Test
    public void testFormatShortServerDate() {
        try {
            String actualValue = dateManager.formatShortServerDate(dateValue);
            assertEquals(shortServerFormatDateString, actualValue);
        } catch (Exception exception) {
            fail("Exception not handling " + exception.getMessage());
        }
    }

    @Test
    public void testFormatModelDate() {
        try {
            String actualValue = dateManager.formatModelDate(dateValue);
            assertEquals(modelFormatDateString, actualValue);
        } catch (Exception exception) {
            fail("Exception not handling " + exception.getMessage());
        }
    }

    @Test
    public void testFormatShortModelDate() {
        try {
            String actualValue = dateManager.formatShortModelDate(dateValue);
            assertEquals(shortModelFormatDateString, actualValue);
        } catch (Exception exception) {
            fail("Exception not handling " + exception.getMessage());
        }
    }

    @Test
    public void testFormatCustomDate() {
        try {
            String actualValue = dateManager.formatCustomDate(dateValue, customDateFormat);
            assertEquals(customFormatDateString, actualValue);
        } catch (Exception exception) {
            fail("Exception not handling " + exception.getMessage());
        }
    }

    @Test
    public void testParseServerDate() {
        try {
            Date actualValue = dateManager.parseServerDate(serverFormatDateString);
            assertEquals(dateValue, actualValue);
        } catch (Exception exception) {
            fail("Exception not handling " + exception.getMessage());
        }
    }

    @Test
    public void testParseShortServerDate() {
        try {
            Date actualValue = dateManager.parseShortServerDate(shortServerFormatDateString);
            Calendar calendar = GregorianCalendar.getInstance();
            if (dateValue != null) {
                calendar.setTime(dateValue);
                calendar.set(Calendar.HOUR, 0);
                calendar.set(Calendar.MINUTE, 0);
                calendar.set(Calendar.SECOND, 0);
                calendar.set(Calendar.HOUR_OF_DAY, 0);
                assertEquals(calendar.getTime(), actualValue);
            } else {
                assertEquals(null, actualValue);
            }
        } catch (Exception exception) {
            fail("Exception not handling " + exception.getMessage());
        }
    }

    @Test
    public void testParseModelDate() {
        try {
            Date actualValue = dateManager.parseModelDate(modelFormatDateString);

            Calendar calendar = GregorianCalendar.getInstance();
            if (dateValue != null) {
                calendar.setTime(dateValue);
                calendar.set(Calendar.SECOND, 0);
                assertEquals(calendar.getTime(), actualValue);
            } else {
                assertEquals(null, actualValue);
            }

        } catch (Exception exception) {
            fail("Exception not handling " + exception.getMessage());
        }
    }

    @Test
    public void testParseShortModelDate() {
        try {
            Date actualValue = dateManager.parseShortModelDate(shortModelFormatDateString);

            Calendar calendar = GregorianCalendar.getInstance();
            if (dateValue != null) {
                calendar.setTime(dateValue);
                calendar.set(Calendar.HOUR, 0);
                calendar.set(Calendar.MINUTE, 0);
                calendar.set(Calendar.SECOND, 0);
                calendar.set(Calendar.HOUR_OF_DAY, 0);
                assertEquals(calendar.getTime(), actualValue);
            } else {
                assertEquals(null, actualValue);
            }

        } catch (Exception exception) {
            fail("Exception not handling " + exception.getMessage());
        }
    }

    @Test
    public void testParseCustomDate() {
        try {
            Date actualValue = dateManager.parseCustomDate(serverFormatDateString, DateFormatter.defaultFormatMap.get(FormatType.DEFAULT_SERVER_DATE_FORMAT));
            assertEquals(dateValue, actualValue);
        } catch (Exception exception) {
            fail("Exception not handling " + exception.getMessage());
        }
    }

    public static Calendar getDate(int year, int month, int day) {
        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.YEAR, year);
        cal.set(Calendar.MONTH, month);
        cal.set(Calendar.DAY_OF_MONTH, day);
        cal.set(Calendar.HOUR_OF_DAY, 0);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.SECOND, 0);
        cal.set(Calendar.MILLISECOND, 0);
        return cal;
    }

}
