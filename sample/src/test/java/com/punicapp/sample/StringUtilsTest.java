package com.punicapp.sample;

import com.punicapp.core.utils.StringUtils;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Arrays;
import java.util.Collection;

import static org.junit.Assert.*;

/**
 * Created by Evgeny on 17.01.18.
 */
@RunWith(Parameterized.class)
public class StringUtilsTest {

    @Parameterized.Parameter(value = 0)
    public String inputTestData;

    @Parameterized.Parameter(value = 1)
    public String outputTestData;

    @Parameterized.Parameters
    public static Collection<Object[]> initParameters() {
        return Arrays.asList(new Object[][] {
                {"wbcschqv", "Wbcschqv" },
                { "Ac nbvh", "Ac nbvh" },
                { "_vnnqsn", "_vnnqsn" },
                {".Fbbhqkxn sscb", ".Fbbhqkxn sscb"},
                {"@ndbasb", "@ndbasb"},
                {"1abc", "1abc"},
                {null, ""},
                {"", ""},
                {"ädnsjnc", "Ädnsjnc"},
                {"万111", "万111"}});
    }

    @Test
    public void testCapitalizeCorrectness() {
        try {
            assertEquals(outputTestData, StringUtils.capitalize(inputTestData));
        } catch(Exception exception) {
            fail("Exception not handling! " + exception.getMessage());
        }
    }

}
