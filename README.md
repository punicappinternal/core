# gradle-simple

[![](https://jitpack.io/v/org.bitbucket.punicappinternal/core.svg)](https://jitpack.io/#org.bitbucket.punicappinternal/core)

Punicapp core library (includes just some utils for now), which is used in internal projects.

To install the library add: 
 
   ```gradle
   allprojects {
       repositories {
           maven { url 'https://jitpack.io' }
       }
   }
   dependencies {
           compile 'org.bitbucket.punicappinternal:core:-SNAPSHOT'
   }
   ```

Under [MIT License](https://bitbucket.org/punicappinternal/core/src/master/LICENSE)
