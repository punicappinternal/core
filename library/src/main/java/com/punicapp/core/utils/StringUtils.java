package com.punicapp.core.utils;

public class StringUtils {
    public static String capitalize(String string) {
        if (string == null) {
            return "";
        }
        if (!string.isEmpty()) {
            StringBuilder stringBuilder = new StringBuilder(string);
            stringBuilder.setCharAt(0, Character.toUpperCase(stringBuilder.charAt(0)));
            return stringBuilder.toString();
        } else {
            return "";
        }
    }
}
