package com.punicapp.core.utils;

import java.text.DateFormat;
import java.text.ParseException;
import java.util.Date;

/**
 * Created by Evgeny on 25.01.18.
 */

public class DateManager implements IDateManager {
    private static DateManager instance;

    private DateManager() {
        DateFormatter.newBuilder().build();
    }

    public static DateManager getInstance() {
        if (instance == null) {
            instance = new DateManager();
        }
        return instance;
    }

    @Override
    public String formatServerDate(Date date) {
        return formatValue(date, DateFormatter.defaultFormatMap.get(FormatType.DEFAULT_SERVER_DATE_FORMAT));
    }

    @Override
    public String formatShortServerDate(Date date) {
        return formatValue(date, DateFormatter.defaultFormatMap.get(FormatType.DEFAULT_SHORT_SERVER_DATE_FORMAT));
    }

    @Override
    public String formatModelDate(Date date) {
        return formatValue(date, DateFormatter.defaultFormatMap.get(FormatType.DEFAULT_MODEL_DATE_FORMAT));
    }

    @Override
    public String formatShortModelDate(Date date) {
        return formatValue(date, DateFormatter.defaultFormatMap.get(FormatType.DEFAULT_SHORT_MODEL_DATE_FORMAT));
    }

    @Override
    public String formatCustomDate(Date date, DateFormat dateFormat) {
        return formatValue(date, dateFormat);
    }

    @Override
    public Date parseServerDate(String dateString) {
        return parseValue(dateString, DateFormatter.defaultFormatMap.get(FormatType.DEFAULT_SERVER_DATE_FORMAT));
    }

    @Override
    public Date parseShortServerDate(String dateString) {
        return parseValue(dateString, DateFormatter.defaultFormatMap.get(FormatType.DEFAULT_SHORT_SERVER_DATE_FORMAT));
    }

    @Override
    public Date parseModelDate(String dateString) {
        return parseValue(dateString, DateFormatter.defaultFormatMap.get(FormatType.DEFAULT_MODEL_DATE_FORMAT));
    }

    @Override
    public Date parseShortModelDate(String dateString) {
        return parseValue(dateString, DateFormatter.defaultFormatMap.get(FormatType.DEFAULT_SHORT_MODEL_DATE_FORMAT));
    }

    @Override
    public Date parseCustomDate(String dateString, DateFormat dateFormat) {
        return parseValue(dateString, dateFormat);
    }

    private String formatValue(Date date, DateFormat dateFormat) {
        String dateString;
        if (date != null && dateFormat != null) {
            dateString = dateFormat.format(date);
        } else {
            dateString = "";
        }
        return dateString;
    }

    private Date parseValue(String dateString, DateFormat dateFormat) {
        Date date;
        if (dateString != null && !dateString.isEmpty()) {
            try {
                date = dateFormat.parse(dateString);
            } catch (ParseException exception) {
                date = null;
            }
        } else {
            date = null;
        }
        return date;
    }
}
