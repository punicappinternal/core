package com.punicapp.core.utils;

import java.text.DateFormat;
import java.util.Date;

/**
 * Created by Evgeny on 25.01.18.
 */

public interface IDateManager {
    String formatServerDate(Date date);

    String formatShortServerDate(Date date);

    String formatModelDate(Date date);

    String formatShortModelDate(Date date);

    String formatCustomDate(Date date, DateFormat dateFormat);

    Date parseServerDate(String dateString);

    Date parseShortServerDate(String dateString);

    Date parseModelDate(String dateString);

    Date parseShortModelDate(String dateString);

    Date parseCustomDate(String dateString, DateFormat dateFormat);
}
