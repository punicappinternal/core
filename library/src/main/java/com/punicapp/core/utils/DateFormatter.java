package com.punicapp.core.utils;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.EnumMap;

/**
 * Created by Evgeny on 25.01.18.
 */

public class DateFormatter {
    public static final EnumMap<FormatType, DateFormat> defaultFormatMap = new EnumMap<>(FormatType.class);

    private DateFormat serverDateFormat;
    private DateFormat shortServerDateFormat;
    private DateFormat modelDateFormat;
    private DateFormat shortModelDateFormat;

    private DateFormatter() {
        defaultFormatMap.put(FormatType.values()[0], new SimpleDateFormat("yyyy-MM-dd HH:mm:ss"));
        defaultFormatMap.put(FormatType.values()[1], new SimpleDateFormat("yyyy-MM-dd"));
        defaultFormatMap.put(FormatType.values()[2], new SimpleDateFormat("dd.MM.yyyy HH:mm"));
        defaultFormatMap.put(FormatType.values()[3], new SimpleDateFormat("dd.MM.yyyy"));

        serverDateFormat = defaultFormatMap.get(FormatType.DEFAULT_SERVER_DATE_FORMAT);
        shortServerDateFormat = defaultFormatMap.get(FormatType.DEFAULT_SHORT_SERVER_DATE_FORMAT);
        modelDateFormat = defaultFormatMap.get(FormatType.DEFAULT_MODEL_DATE_FORMAT);
        shortModelDateFormat = defaultFormatMap.get(FormatType.DEFAULT_SHORT_MODEL_DATE_FORMAT);
    }

    public DateFormat getServerDateFormat() {
        return serverDateFormat;
    }

    public DateFormat getShortServerDateFormat() {
        return shortServerDateFormat;
    }

    public DateFormat getModelDateFormat() {
        return modelDateFormat;
    }

    public DateFormat getShortModelDateFormat() {
        return shortModelDateFormat;
    }

    public static DateFormatterBuilder newBuilder() {
        return new DateFormatter().new DateFormatterBuilder();
    }

    public class DateFormatterBuilder {
        private DateFormatterBuilder() {

        }

        public DateFormatterBuilder setServerDateFormat(DateFormat serverDateFormat) {
            DateFormatter.this.serverDateFormat = serverDateFormat;
            return this;
        }

        public DateFormatterBuilder setShortServerDateFormat(DateFormat shortServerDateFormat) {
            DateFormatter.this.shortServerDateFormat = shortServerDateFormat;
            return this;
        }

        public DateFormatterBuilder setModelDateFormat(DateFormat modelDateFormat) {
            DateFormatter.this.modelDateFormat = modelDateFormat;
            return this;
        }

        public DateFormatterBuilder setShortModelDateFormat(DateFormat shortModelDateFormat) {
            DateFormatter.this.shortModelDateFormat = shortModelDateFormat;
            return this;
        }

        public DateFormatter build() {
            return DateFormatter.this;
        }
    }

}
