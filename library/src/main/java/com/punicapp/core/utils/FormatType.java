package com.punicapp.core.utils;

/**
 * Created by Evgeny on 25.01.18.
 */

public enum FormatType {
    DEFAULT_SERVER_DATE_FORMAT,
    DEFAULT_SHORT_SERVER_DATE_FORMAT,
    DEFAULT_MODEL_DATE_FORMAT,
    DEFAULT_SHORT_MODEL_DATE_FORMAT
}
