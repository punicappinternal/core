package com.punicapp.core.network;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import io.reactivex.Observable;
import io.reactivex.subjects.PublishSubject;

public class NetworkStateReceiver extends BroadcastReceiver {

    private Context context;
    private PublishSubject<Boolean> subject;


    public NetworkStateReceiver(Context context) {
        this.context = context;
        subject = PublishSubject.create();
    }

    public void register() {
        IntentFilter intentFilter = new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION);
        context.registerReceiver(this, intentFilter);
    }

    public void unregister() {
        context.unregisterReceiver(this);
    }

    public boolean checkConnectedToInternet(Context context) {
        if (context == null) {
            return false;
        }
        ConnectivityManager connectivity = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        if (connectivity == null) {
            return false;
        }
        NetworkInfo info = connectivity.getActiveNetworkInfo();

        if (info == null) {
            return false;
        }
        if (info.isAvailable() && info.isConnected())
            return true;

        return false;
    }

    public boolean checkConnectionManual() {
        boolean isConnected = checkConnectedToInternet(context);

        if (!isConnected) {
            subject.onNext(isConnected);
        }

        return isConnected;
    }

    @Override
    public void onReceive(Context context, Intent intent) {     // registerReceiver callback, passed to mListener
        boolean isConnected = checkConnectedToInternet(this.context);
        subject.onNext(isConnected);
    }

    public Observable<Boolean> listen() {
        return subject;
    }
}